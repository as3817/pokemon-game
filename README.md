README: 

Pokemon game project done for Design Patterns. Team members include: Adam Standke, Stephen Clabaugh II, Gegory Whitman, and Gabriel Webbe.
Created a fully functional game in which two players can each choose three pokemon from three different pokemon classes and then battle each other
in typical pokemon game fashion. The battle moves include attacking or switching the current pokemon, in which case the player loses his or her turn.The
game is fully functional and after a player's three pokemon are completly dead, a winner is crowned. Game was created using the java language. 
	
	For this team project, I was primarily responsible for classes contained within the following packages:maininterface, observer, players, state, 
	and command.Also, I was tasked with designing the UML for how the system would run and how the various software components/interfaces would
	interact with each other. 
